package com.newrelic.codingchallenge.noduplicate;

import org.apache.commons.lang3.math.NumberUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

import static com.newrelic.codingchallenge.noduplicate.CommonConstants.TERMINATE;

/**
 * created by Stephen Lang on 7/11/2020
 * <p>
 * A stateless Handles data stream
 * System.out.println("Thread:" + Thread.currentThread().getId() +
 * " count:" + count++ + " received " + received);
 **/
public class Handler implements Runnable {
    private final ServerSocket serverSocket;
    private final LogAndReportService logAndReportService;

    Handler(ServerSocket serverSocket, LogAndReportService logAndReportService) throws IOException {
        this.serverSocket = serverSocket;
        this.logAndReportService = logAndReportService;
    }

    @Override
    public void run() {

        try (Socket socket = this.serverSocket.accept()) {
            //            System.out.println("Thread " + Thread.currentThread().getId() + " accepting input.");
            try (BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()))) {
                int count = 0;
                while (!NoDuplicateServer.shutDownNow) {
                    String received = in.readLine();
                    count++;
                    if (TERMINATE.equals(received)) {
//                    System.out.println("Thread:" + Thread.currentThread().getId() +
//                            " count:" + count + " received " + received);
                        NoDuplicateServer.shutDownNow = true;
                        break;
                    }
                    if (!NumberUtils.isDigits(received)) {
                        break;
                    }
                    logAndReportService.add(Integer.valueOf(received));
                }
//            System.out.println("Thread " + Thread.currentThread().getId() + " exit...");
            } catch (IOException e) {
                Thread.currentThread().interrupt();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
