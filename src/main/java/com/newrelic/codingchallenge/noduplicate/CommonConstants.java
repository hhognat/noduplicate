package com.newrelic.codingchallenge.noduplicate;

/**
 * created by Stephen Lang on 7/11/2020
 **/
public class CommonConstants {
    public static final int PORT = 4000;
    public static final String TERMINATE = "terminate";
    public static final int POOL_SIZE = 5;

    public static final String NUMBERS_LOG = "numbers.log";
}
