package com.newrelic.codingchallenge.noduplicate;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Set;

/**
 * created by Stephen Lang on 7/12/2020
 * noDuplicateSet = Collections.synchronizedSet(new HashSet<Integer>(initialCapacity));
 **/
public class LogAndReportService {
    private final Set<Integer> noDuplicateSet;
    public static final int initialCapacity = 100000000;
    public static final long reportPeriod = 10000; // 10 seconds
    private final PrintWriter out;
    private int numberOfDuplicate = 0;
    private long uniqueTotal = 0;
    private int previousSize = 0;
    public LogAndReportService() {
        try {
            int bufferSize = 32 * 1024;
            BufferedWriter bufferedWriter =
                    new BufferedWriter(new FileWriter(CommonConstants.NUMBERS_LOG), bufferSize);
            out = new PrintWriter(bufferedWriter);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        noDuplicateSet = new HashSet<Integer>(initialCapacity);
        new Thread(this::report).start();
    }

    private void report() {
        while (!NoDuplicateServer.shutDownNow) {
            try {
                Thread.sleep(reportPeriod);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
            int sizeNow = noDuplicateSet.size();
            String report = "";
            if (previousSize == 0) {
                report = String.format("Received %,d unique numbers, %,d duplicates. Unique total: %,d",
                        sizeNow, numberOfDuplicate, uniqueTotal);
            } else {
                report = String.format("Received %,d unique numbers added %,d, %,d duplicates. Unique total: %,d",
                        sizeNow, sizeNow - previousSize, numberOfDuplicate, uniqueTotal);
            }
            previousSize = sizeNow;
            System.out.println(report);
        }
    }

    public synchronized void add(Integer received) {
        if (!noDuplicateSet.add(received)) {
            numberOfDuplicate++;
        } else {
            uniqueTotal += received;
            if (!NoDuplicateApplication.argsSet.contains("nolog")) {
                String data = String.valueOf(received);
                out.println(data);
            }
        }
    }
}
