package com.newrelic.codingchallenge.noduplicate;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;


/**
 * created by Stephen Lang on 7/11/2020
 * A socket server dispatches a thread per socket connection from a fixed pool.
 * Performs graceful shutdown when all threads are finished.
 **/
public class NoDuplicateServer extends Thread {
    public static boolean shutDownNow = false;
    private final ServerSocket serverSocket;
    private final ExecutorService executorService;
    private final LogAndReportService logAndReportService;
    private final int poolSize;

    public NoDuplicateServer(int port, int poolSize) throws IOException {
        this.poolSize = poolSize;
        this.executorService = Executors.newWorkStealingPool(poolSize);
        this.serverSocket = new ServerSocket(port, 1);
        this.logAndReportService = new LogAndReportService();
    }

    @Override
    public void run() {
        System.out.println("Starting up server ....");
        try {
            Handler handler = new Handler(serverSocket, logAndReportService);
            while (!shutDownNow) {
                if (((ForkJoinPool) executorService).getActiveThreadCount() < poolSize) {
                    executorService.execute(handler);
                } else {
                    Thread.sleep(1000);
                }
            }
            shutdownAndAwaitTermination();
        } catch (IOException ex) {
            shutdownAndAwaitTermination();
        } catch (RejectedExecutionException re) {
            System.out.println("Shutting down.");
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    void shutdownAndAwaitTermination() {
        executorService.shutdown();
        try {
            if (!executorService.awaitTermination(1, TimeUnit.SECONDS)) {
                executorService.shutdownNow(); // Cancel currently executing tasks
                if (!executorService.awaitTermination(1, TimeUnit.SECONDS))
                    executorService.shutdown();
            }
        } catch (InterruptedException ie) {
            executorService.shutdownNow();
            Thread.currentThread().interrupt();
        }
    }

    public boolean getShutDownNow() {
        return shutDownNow;
    }

    public ExecutorService getExecutorService() {
        return executorService;
    }
}
