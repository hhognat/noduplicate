package com.newrelic.codingchallenge.noduplicate;


import com.newrelic.codingchallenge.noduplicate.client.ClientApplication;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static com.newrelic.codingchallenge.noduplicate.CommonConstants.POOL_SIZE;
import static com.newrelic.codingchallenge.noduplicate.CommonConstants.PORT;

public class NoDuplicateApplication {
    public static Set<String> argsSet;
    public static void main(String[] args) throws IOException, InterruptedException {
        argsSet = new HashSet<String>(Arrays.asList(args));
        if (argsSet.contains("client")) {
            ClientApplication.main(args);
        } else {
            NoDuplicateServer noDuplicateServer = new NoDuplicateServer(PORT, POOL_SIZE);
            noDuplicateServer.start();
        }
    }

}
