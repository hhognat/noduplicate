package com.newrelic.codingchallenge.noduplicate.client;

import org.apache.commons.math3.random.JDKRandomGenerator;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;

/**
 * created by Stephen Lang on 7/12/2020
 **/
public class DataProducerLoop implements DataProducer {
    public static boolean finished = false;
    private final int loopCount;
    private final InetAddress ip;

    public DataProducerLoop(int loopCount, InetAddress ip) {
        this.loopCount = loopCount;
        this.ip = ip;
    }

    @Override
    public void sendDataToServer(int loopCount, Socket s) {
        JDKRandomGenerator randomGenerator = new JDKRandomGenerator();

        try (PrintWriter out = new PrintWriter(s.getOutputStream(), true)) {
            int countSent = 0;
            for (int i = 0; i < loopCount; i++) {
                int randomInt = randomGenerator.nextInt(99999999);
                String data = String.format("%09d", randomInt);
                out.println(data);
                countSent++;
                if (DataProducerLoop.finished) {
                    break;
                }
            }
            String message =
                    String.format("Thread %d finished sending:%,d",Thread.currentThread().getId(),countSent);
            System.out.println(message);
            finished = true;
        } catch (IOException e) {
            System.err.println("Thread " + Thread.currentThread().getId() + " write failed");
        }
    }

    @Override
    public void run() {
        runit(loopCount, ip);
    }
}
