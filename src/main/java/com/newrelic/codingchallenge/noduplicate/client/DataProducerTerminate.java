package com.newrelic.codingchallenge.noduplicate.client;

import com.newrelic.codingchallenge.noduplicate.CommonConstants;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;

/**
 * created by Stephen Lang on 7/12/2020
 **/
public class DataProducerTerminate implements DataProducer {
    private final int loopCount;
    private final InetAddress ip;

    public DataProducerTerminate(int loopCount, InetAddress ip) {
        this.loopCount = loopCount;
        this.ip = ip;
    }

    @Override
    public void sendDataToServer(int loopCount, Socket s) throws IOException {
        try (PrintWriter out = new PrintWriter(s.getOutputStream(), true)) {
            out.println(CommonConstants.TERMINATE);
            System.out.println("Send to server:" + CommonConstants.TERMINATE);
        }
    }

    @Override
    public void run() {

    }
}
