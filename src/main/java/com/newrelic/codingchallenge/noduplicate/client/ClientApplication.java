package com.newrelic.codingchallenge.noduplicate.client;

import java.io.IOException;
import java.net.InetAddress;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static com.newrelic.codingchallenge.noduplicate.CommonConstants.POOL_SIZE;

/**
 * created by Stephen Lang on 7/11/2020
 **/
public class ClientApplication {

    public static void main(String[] args) throws IOException, InterruptedException {
        int loopCount = 10000000;
        int poolSizeMultiplier = 2;
        int clientPoolSize = POOL_SIZE * poolSizeMultiplier;
        InetAddress ip = InetAddress.getByName("localhost");
        ExecutorService executorService = Executors.newFixedThreadPool(clientPoolSize);
        DataProducerLoop dataProducerLoop = new DataProducerLoop(loopCount, ip);
        for (int i = 0; i < clientPoolSize; i++) {
            executorService.execute(dataProducerLoop);
        }
        try {
            executorService.shutdown();
            while (!DataProducerLoop.finished) {
                Thread.sleep(1000);
            }
            executorService.shutdownNow();
            if (!executorService.awaitTermination(5, TimeUnit.SECONDS)) {
                executorService.shutdownNow();
            }
            DataProducer dataProducerTerminate = new DataProducerTerminate(loopCount, ip);
            for (int i = 0; i < clientPoolSize; i++) {
                dataProducerTerminate.runit(loopCount, ip);
            }
        } catch (InterruptedException ex) {
            System.err.println("Thread " + Thread.currentThread().getId() + " InterruptedException ");
            executorService.shutdownNow();
            Thread.currentThread().interrupt();
        }
    }

}
