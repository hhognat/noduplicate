package com.newrelic.codingchallenge.noduplicate.client;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

import static com.newrelic.codingchallenge.noduplicate.CommonConstants.PORT;

public interface DataProducer extends Runnable {
    void sendDataToServer(int loopCount, Socket s) throws IOException;

    default void runit(int loopCount, InetAddress ip) {
        try (Socket s = new Socket(ip, PORT)) {
            sendDataToServer(loopCount, s);
        } catch (IOException e) {
            System.err.println("Thread " + Thread.currentThread().getId() + " " + e.getMessage());
        }
    }
}
