## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)

## General info
This Java application is a command line application which is a Socket Server and a Socket client for localhost

The server only allows 5 concurrent connections. The server starts with 5 threads. 
Each thread terminates after receiving a "terminate" word.

The client starts with 10 threads. Some connections will be refused.
Each client thread sends 10,000,000 (10 million) numbers and then send "terminate".

 	
## Technologies
Project uses:
* Maven build tool
* Apache Commons Math and Lang for increased productivity
## Setup
* Java 11 needs to installed

## To build this project:
```
$ chmod +x ./mvnw 
$ ./mvnw package
```
## To run this project as a Socket Server:
```
$ java -jar target/noduplicate-0.0.1-SNAPSHOT.jar 
```
* Results are in numlogs.log file

## To run this project as a Client:
```
$ java -jar target/noduplicate-0.0.1-SNAPSHOT.jar client
```
* The client has some console logs

#Tested on Windows and Ubuntu:
Windows:
Starting up server ....
Received 1,431,454 unique numbers, 10,371 duplicates. Unique total: 71,528,949,407,520
Received 4,808,855 unique numbers added 3,377,401, 119,996 duplicates. Unique total: 240,405,429,973,484
Received 8,080,022 unique numbers added 3,271,167, 346,018 duplicates. Unique total: 404,063,475,233,956
Received 11,172,305 unique numbers added 3,092,283, 676,073 duplicates. Unique total: 558,737,561,404,112
Received 14,216,050 unique numbers added 3,043,745, 1,120,523 duplicates. Unique total: 710,979,669,332,653
Received 17,146,358 unique numbers added 2,930,308, 1,665,821 duplicates. Unique total: 857,456,664,282,817
Received 19,814,724 unique numbers added 2,668,366, 2,271,313 duplicates. Unique total: 990,873,882,015,688
Received 21,975,306 unique numbers added 2,160,582, 2,843,060 duplicates. Unique total: 1,098,974,520,919,641
Received 23,305,001 unique numbers added 1,329,695, 3,231,507 duplicates. Unique total: 1,165,408,634,648,517
Received 25,600,031 unique numbers added 2,295,030, 3,975,101 duplicates. Unique total: 1,280,124,967,537,629
Received 27,894,720 unique numbers added 2,294,689, 4,813,035 duplicates. Unique total: 1,394,809,466,723,596
Received 29,494,873 unique numbers added 1,600,153, 5,456,585 duplicates. Unique total: 1,474,932,236,295,239
Received 31,726,176 unique numbers added 2,231,303, 6,440,745 duplicates. Unique total: 1,586,565,081,831,289
Received 33,838,013 unique numbers added 2,111,837, 7,471,195 duplicates. Unique total: 1,692,143,096,877,762
Received 35,272,046 unique numbers added 1,434,033, 8,228,137 duplicates. Unique total: 1,763,874,466,039,418
Received 36,975,047 unique numbers added 1,703,001, 9,190,768 duplicates. Unique total: 1,849,046,715,015,338
Received 38,527,620 unique numbers added 1,552,573, 10,131,943 duplicates. Unique total: 1,926,677,731,289,180
Received 38,966,287 unique numbers added 438,667, 10,408,584 duplicates. Unique total: 1,948,626,370,748,817

Ubuntu:
Starting up server ....
Received 0 unique numbers, 0 duplicates. Unique total: 0
Received 0 unique numbers, 0 duplicates. Unique total: 0
Received 3,760,201 unique numbers, 72,223 duplicates. Unique total: 188,046,411,046,872
Received 12,531,014 unique numbers added 8,770,813, 857,613 duplicates. Unique total: 626,449,187,093,345
Received 20,745,762 unique numbers added 8,214,748, 2,508,073 duplicates. Unique total: 1,037,281,108,304,232
Received 28,302,770 unique numbers added 7,557,008, 4,973,234 duplicates. Unique total: 1,414,973,270,617,966
Received 33,145,541 unique numbers added 4,842,771, 7,123,200 duplicates. Unique total: 1,657,187,523,698,690
Received 37,443,062 unique numbers added 4,297,521, 9,468,154 duplicates. Unique total: 1,872,037,350,197,630
Received 37,443,062 unique numbers added 0, 9,468,154 duplicates. Unique total: 1,872,037,350,197,630


